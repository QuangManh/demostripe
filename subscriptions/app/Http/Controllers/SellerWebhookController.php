<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Seller;
use Laravel\Cashier\Http\Controllers\WebhookController;

class SellerWebhookController extends Controller
{
    public function __construct()
    {
        $this->middleware('seller');
        $this->stripe = new \Stripe\StripeClient(
            'sk_test_51L5PKcHUMb6OyrHrRSgsEVoWBfjkyCHvIlMTZ13pBFOofj9ITVJ3vBX5r1rxu9oMYhTf0ddPIEJPirt8dmvFZYa6002IMvSedy'
        );
    }

    //
    protected function getUserByStripeId($stripeId)
    {
        return Seller::where('stripe_id', $stripeId)->first();
    }

    public function home (){
        return view('sell.sell_home');
    }

    public function plan (){
        $product = $this->stripe->products->all();
        $arr =array();
        foreach ($product as $item){
            $a = $this->stripe->prices->retrieve(
                $item->default_price,
                []
            );
            $item->unit_amount_decimal = $a->unit_amount_decimal;
            $item->interval = $a->recurring;
            array_push($arr, $item);
        }
        return view('sell.all_plan', compact( 'arr'));
    }

    public function checkout ($id){
        $plan = $this->stripe->products->retrieve($id,[]);
        $a = $this->stripe->prices->retrieve( $plan->default_price, [] );
        $plan->unit_amount_decimal = $a->unit_amount_decimal;
        $plan->interval = $a->recurring;

        $b = auth()->guard('seller')->user()->createSetupIntent();

        return view('sell.checkout', [
            'plan' => $plan,
            'intent' => $b,
        ]);
    }
    public function process(Request $request){
        $user = auth()->guard('seller')->user();
        $user->createOrGetStripeCustomer();
        $paymentMethod = null;
        $paymentMethod = $request->payment_method;
        if($paymentMethod != null){
            $paymentMethod = $user->addPaymentMethod($paymentMethod);
        }
        $default_price = $request->default_price;
        $user->newSubscription(
            'default', $default_price
        )->create($paymentMethod->id);

        dd('Thanh Toán thành công');
    }
}
