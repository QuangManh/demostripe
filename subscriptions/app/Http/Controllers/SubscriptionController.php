<?php

namespace App\Http\Controllers;

use App\Models\Plan as ModelsPlan;
use Exception;
use Illuminate\Http\Request;
use Stripe\Plan;

class SubscriptionController extends Controller
{
    //

    public function showPlanForm()
    {
        return view('stripe.plans.create');
    }

    public function savePlan(Request $request)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $amount = ($request->amount * 100);

        try {
            $plan = Plan::create([
                'amount' => $amount,
                'currency' => $request->currency,
                'interval' => $request->billing_period,
                'interval_count' => $request->interval_count,
                'product' => [
                    'name' => $request->name
                ]
            ]);

            ModelsPlan::create([
                'plan_id' => $plan->id,
                'name' => $request->name,
                'price' => $plan->amount,
                'billing_method' => $plan->interval,
                'currency' => $plan->currency,
                'interval_count' => $plan->interval_count
            ]);


        }
        catch(Exception $ex){
            dd($ex->getMessage());
        }

        return "success";
    }

    public function allPlans()
    {
        $stripe = new \Stripe\StripeClient(
            'sk_test_51L5PKcHUMb6OyrHrRSgsEVoWBfjkyCHvIlMTZ13pBFOofj9ITVJ3vBX5r1rxu9oMYhTf0ddPIEJPirt8dmvFZYa6002IMvSedy'
        );
        $product = $stripe->products->all();
//        dd($product);
        $arr =array();
        foreach ($product as $item){
            $a = $stripe->prices->retrieve(
                $item->default_price,
                []
            );
            $item->unit_amount_decimal = $a->unit_amount_decimal;
            $item->interval = $a->recurring;
            array_push($arr, $item);
        }
        return view('stripe.plans', compact( 'arr'));
    }

    public function checkout($planId)
    {

        $stripe = new \Stripe\StripeClient(
            'sk_test_51L5PKcHUMb6OyrHrRSgsEVoWBfjkyCHvIlMTZ13pBFOofj9ITVJ3vBX5r1rxu9oMYhTf0ddPIEJPirt8dmvFZYa6002IMvSedy'
        );
        $plan = $stripe->products->retrieve(
            $planId,
            []
        );
        $a = $stripe->prices->retrieve(
            $plan->default_price,
            []
        );
        $plan->unit_amount_decimal = $a->unit_amount_decimal;
        $plan->interval = $a->recurring;
        return view('stripe.plans.checkout', [
            'plan' => $plan,
            'intent' => auth()->user()->createSetupIntent(),
        ]);
    }

    public function processPlan(Request $request)
    {
        $user = auth()->user();
        $user->createOrGetStripeCustomer();
        $paymentMethod = null;
        $paymentMethod = $request->payment_method;
        if($paymentMethod != null){
            $paymentMethod = $user->addPaymentMethod($paymentMethod);
        }
        $default_price = $request->default_price;
        $user->newSubscription(
            'default', $default_price
        )->create($paymentMethod->id);
        dd('Thanh Toán thành công');
//        $request->session()->flash('alert-success', 'You are subscribed to this plan');
//        return redirect()->route('plans.checkout', compact('plan'));
    }


}
