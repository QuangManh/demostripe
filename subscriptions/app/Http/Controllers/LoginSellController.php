<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginSellController extends Controller
{
    //

    public function login(){
        return view('sell_login');
    }
    public function login_post(Request $request){
        $arr = [
            'email' => $request->email,
            'password' => $request->password
        ];
        Auth::guard('seller')->attempt($arr);
        return redirect()->route('sell.home');
    }

    public function logout(Request $request){
        Auth::guard('seller')->logout();
        return redirect()->route('sell.login');
    }
}
