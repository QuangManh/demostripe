<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Cashier\Subscription;

class SellerSubscription extends Subscription
{
    use HasFactory;
    public function owner()
    {
        return $this->belongsTo(Seller::class);
    }
    public function items()
    {
        return $this->hasMany(SellerSubscriptionItem::class);
    }
}
