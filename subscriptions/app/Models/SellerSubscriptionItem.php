<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\SubscriptionItem;

class SellerSubscriptionItem extends SubscriptionItem
{
    use HasFactory;
    public function subscription()
    {
        return $this->belongsTo(SellerSubscription::class);
    }
}
