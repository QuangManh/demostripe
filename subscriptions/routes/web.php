<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\SellerWebhookController;
use App\Http\Controllers\LoginSellController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::post('single-charge', [HomeController::class, 'singleCharge'])->name('single.charge');

Route::get('plans/create', [SubscriptionController::class, 'showPlanForm'])->name('plans.create');
Route::post('plans/store', [SubscriptionController::class, 'savePlan'])->name('plans.store');

Route::get('plans', [SubscriptionController::class, 'allPlans'])->name('plans.all');
Route::get('plans/checkout/{planId}', [SubscriptionController::class, 'checkout'])->name('plans.checkout');
Route::post('plans/process', [SubscriptionController::class, 'processPlan'])->name('plan.process');


//***********************sell

Route::post('/stripe/seller/webhook', [SellerWebhookController::class, 'handleWebhook']);


//***************************

Route::get('/sell/login', [LoginSellController::class, 'login'])->name('sell.login');
Route::post('/sell/login', [LoginSellController::class, 'login_post'])->name('sell.login.post');

Route::get('/sell/home', [SellerWebhookController::class, 'home'])->name('sell.home');
Route::get('/sell/logout', [LoginSellController::class, 'logout'])->name('sell.logout');
Route::get('/sell/plan', [SellerWebhookController::class, 'plan'])->name('sell.plan');
Route::get('/sell/plan/{id}', [SellerWebhookController::class, 'checkout'])->name('sell.checkout');
Route::post('/sell/process', [SellerWebhookController::class, 'process'])->name('sell.process');

