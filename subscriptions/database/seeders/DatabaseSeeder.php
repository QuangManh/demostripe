<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('1'),
            ]
        ]);

        DB::table('sellers')->insert([
            [
                'name' => 'user',
                'email' => 'user@gmail.com',
                'password' => bcrypt('1'),
            ]
        ]);
    }
}
